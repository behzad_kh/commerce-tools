# Getting Started
### For using without docker
1- find "application.properties" and change "mysqldb:3306" to any port or ip you would like to.<br/>
2- please set the user and password in "application.properties".<br/>
3- create database in mysql with name of "stock_test".<br/>
4- go to terminal and run "mvn clean package".<br/>
5- run in terminal ->  "java -jar target/CommerceToolsExam-0.0.1-SNAPSHOT.jar".



### Please do the following commands For up and running in Docker 
In linux terminal <br/>
    <ol>
        <li>go to path (the place there is pom.xml)</li>
        <li> run "mvn clean packege"</li>
        <li> run " docker image build -t commerce-tools . " </li>
        <li> run "docker-compose up"</li>
    </ol>
#### you can use the following endpoints to call rest services 
  <ol>
        <li>POST /updateStock</li>
        <li>GET /stock?productId=vegetable-123 </li>
        <li>GET /statistics?time=[today,lastMonth]
            <ol>
                 <li> time values: today, lastmonth </li>
            </ol>
        </li>
    </ol>
