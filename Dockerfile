FROM openjdk:8
VOLUME /tmp
EXPOSE 8080
ADD target/CommerceToolsExam-0.0.1-SNAPSHOT.jar CommerceToolsExam-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","CommerceToolsExam-0.0.1-SNAPSHOT.jar"]