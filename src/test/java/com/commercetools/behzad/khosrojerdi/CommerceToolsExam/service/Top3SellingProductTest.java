package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class Top3SellingProductTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private StockService stockService;

    @Before
    public void initData() {
        IntStream.rangeClosed(1, 10)
                .forEachOrdered(value -> {
                    Product product = new Product("pro" + value, LocalDateTime.now(), null, value);
                    productService.save(product);
                    LocalDateTime timestamp = value > 7 ?
                            LocalDateTime.of(2019, 4, 1, 10, 50) :
                            LocalDateTime.now();
                    Stock stock = new Stock("stock" + value, timestamp, value);
                    stockService.save(stock);
                    product.setStock(stock);
                    stock.setProduct(product);
                    stockService.save(stock);
                });

    }

    @Test
    public void test_basic() {
        LocalDateTime start = LocalDateTime.of(2019, 5, 1, 10, 50);
        LocalDateTime end = LocalDateTime.now();
        List<Product> products = productService.top3SellingProducts(start, end);
        Assert.assertNotNull(products);
        Assert.assertEquals(3, products.size());
    }
}
