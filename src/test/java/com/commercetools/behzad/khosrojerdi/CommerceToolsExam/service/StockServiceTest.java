package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository.StockRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StockServiceTest {

    @InjectMocks
    private StockServiceImpl stockService;

    @Mock
    private StockRepository stockRepo;

    @Test
    public void updateStock_basic() {
        Stock stockDB = new Stock();
        stockDB.setId("s001");
        stockDB.setTimestamp(LocalDateTime.now());
        stockDB.setQuantity(26);

        Stock stock = new Stock();
        stock.setId("s001");
        stock.setTimestamp(LocalDateTime.now());
        stock.setQuantity(40);
        when(stockRepo.findById("s001")).thenReturn(Optional.of(stockDB));
        when(stockRepo.save(stock)).thenReturn(stock);
        assertEquals(stock.getId(), stockService.updateStock(stock).getId());
        assertEquals(stock.getQuantity(), stockService.updateStock(stock).getQuantity());
        assertEquals(stock.getTimestamp(), stockService.updateStock(stock).getTimestamp());

    }

    @Test
    public void updateStock_Null() {
        Stock stock = new Stock();
        stock.setId("s001");
        stock.setTimestamp(LocalDateTime.now());
        stock.setQuantity(40);
        when(stockRepo.findById("s001")).thenReturn(Optional.empty());
//        when(stockRepo.save(stock)).thenReturn(stock);
        assertNull(stockService.updateStock(stock));
    }
}
