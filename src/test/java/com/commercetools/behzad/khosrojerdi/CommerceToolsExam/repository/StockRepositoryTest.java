package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Slf4j
public class StockRepositoryTest {

    @Autowired
    private StockRepository stockRepo;

    @Autowired
    private ProductRepository productRepo;

    private String stockId = "stock0001";
    private String productId = "product0001";


    @Before
    public void setUp() {
        Product product = new Product();
        product.setId(productId);
        product.setItemSold(10);
        product.setRequestTimestamp(LocalDateTime.now());
        productRepo.save(product);

        Stock stock = new Stock();
        stock.setId(stockId);

        stock.setQuantity(20);
        stock.setTimestamp(LocalDateTime.now());
        stock = stockRepo.save(stock);
        product.setStock(stock);
        stock.setProduct(product);
        stockRepo.save(stock);


    }

    @Test
    public void findStockTest() {
        Optional<Stock> stock0001 = stockRepo.findById(stockId);
        assertThat(stock0001).isNotEmpty();
        Stock stock = stock0001.get();
        Assert.assertEquals(stockId, stock.getId());

    }

    @Test
    public void findByProduct_basic() {
        Optional<Product> product0001 = productRepo.findById(productId);
        Stock stock = product0001.get().getStock();//stockRepo.findByProduct("product0001");
        Assert.assertNotNull(stock);
        Assert.assertEquals(stockId, stock.getId());
    }


}
