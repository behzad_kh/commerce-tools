package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.controller;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view.StockView;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service.ProductService;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service.StockServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StockController.class)
public class StockControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockServiceImpl stockService;

    @MockBean
    private ProductService productService;

    @Test
    public void updateStock_basic() throws Exception {
        Stock stock = new Stock();
        stock.setQuantity(23);
        stock.setTimestamp(LocalDateTime.now());
        stock.setId("s001");

        StockView stockView = new StockView("s001", LocalDateTime.now(), "pro001", 26);


        when(stockService.updateStock(any())).thenReturn(stock);

        RequestBuilder postStockUpdate = MockMvcRequestBuilders
                .post("/updateStock")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\" : \"s001\", \"timestamp\" : \"2017-07-16T22:54:01.754Z\", \"productId\" : \"pro001\", \"quantity\" : \"23\"}")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(postStockUpdate)
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void updateStock_NotFoundException() throws Exception {

        when(stockService.updateStock(any())).thenReturn(null);

        RequestBuilder postStockUpdate = MockMvcRequestBuilders
                .post("/updateStock")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\" : \"s001\", \"timestamp\" : \"2017-07-16T22:54:01.754Z\", \"productId\" : \"pro001\", \"quantity\" : \"23\"}")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(postStockUpdate)
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void updateStock_ObjectOptimisticLockingFailureException() throws Exception {
        doThrow(new ObjectOptimisticLockingFailureException("race condition happened", null)).when(stockService).updateStock(any());

        RequestBuilder postStockUpdate = MockMvcRequestBuilders
                .post("/updateStock")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\" : \"s001\", \"timestamp\" : \"2017-07-16T22:54:01.754Z\", \"productId\" : \"pro001\", \"quantity\" : \"23\"}")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(postStockUpdate)
                .andExpect(status().isNoContent())
                .andReturn();
    }

}
