package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository.ProductRepository;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository.StockRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StockServiceIT {

    @Autowired
    private StockService stockService;

    @Autowired
    private StockRepository stockRepo;

    @Autowired
    private ProductRepository productRepo;

    private String stockId = "stock0001";
    private String productId = "product0001";
    private Stock stock;

    @Before
    public void initData() {
        Product product = new Product();
        product.setId(productId);
        product.setItemSold(10);
        product.setRequestTimestamp(LocalDateTime.now());
        productRepo.save(product);

        stock = new Stock();
        stock.setId(stockId);

        stock.setQuantity(20);
        stock.setTimestamp(LocalDateTime.now());
        stock = stockRepo.save(stock);
        product.setStock(stock);
        stock.setProduct(product);
        stockRepo.save(stock);
    }

    @Test
    public void updateStock_basic() {
        stock.setQuantity(100);
        stock.setTimestamp(LocalDateTime.now());
        Stock actualStock = stockService.updateStock(this.stock);
        Assert.assertEquals(this.stock.getQuantity(), actualStock.getQuantity());
        Assert.assertEquals(this.stock.getTimestamp(), actualStock.getTimestamp());
        Assert.assertEquals(this.stock.getProduct().getId(), actualStock.getProduct().getId());
    }


}
