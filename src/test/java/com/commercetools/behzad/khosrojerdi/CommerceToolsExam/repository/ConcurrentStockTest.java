package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ConcurrentStockTest {

    @Autowired
    private StockRepository stockRepo;

    @Autowired
    private ProductRepository productRepo;

    private String stockId = "stock0001";

    @Before
    public void setUp() {
        Product product = new Product();
        product.setId("product0001");
        product.setItemSold(10);
        product.setRequestTimestamp(LocalDateTime.now());
        productRepo.save(product);

        Stock stock = new Stock();
        stock.setId(stockId);
        stock.setQuantity(20);
        stock.setTimestamp(LocalDateTime.now());
        stock = stockRepo.save(stock);
        product.setStock(stock);
        stock.setProduct(product);
        stockRepo.save(stock);

    }

    @Test
    public void test() {
        ExecutorService es = Executors.newFixedThreadPool(2);

        //update 1
        es.execute(() -> {
            try {
                thread(1);

            } catch (ObjectOptimisticLockingFailureException e) {
                log.error(" {}", e.getMessage());
                Assert.assertTrue(true);
            } catch (Exception e) {
                log.error(" {}", e.getClass().toString());
            }
        });
        //update 2
        es.execute(() -> {
            try {
                thread(2);
            } catch (ObjectOptimisticLockingFailureException e) {
                log.error(" {}", e.getMessage());
                Assert.assertTrue(true);
            } catch (Exception e) {
                log.error(" {}", e.getClass().toString());
            }
        });

        es.shutdown();
        try {
            es.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            log.error(" {}", e.getMessage());
        }

    }

    private void thread(int threadNumber) throws Exception {
        log.info(" -- stock{} updating quantity to {}000 --", threadNumber, threadNumber);
        Stock stock = null;

        Optional<Stock> stock0001 = stockRepo.findById("stock0001");
        stock = stock0001.get();
        stock.setQuantity(threadNumber * 1000);
        //little delay

        TimeUnit.SECONDS.sleep(2);

        stockRepo.save(stock0001.get());

        log.info("update{} finished: {}", threadNumber, stock.getQuantity());
    }

    @After
    public void cleanData() {
        stockRepo.deleteAllInBatch();
        productRepo.deleteAllInBatch();
    }
}
