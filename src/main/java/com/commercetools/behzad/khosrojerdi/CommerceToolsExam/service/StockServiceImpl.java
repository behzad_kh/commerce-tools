package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository stockRepo;

    @Override
    public Stock updateStock(Stock stock) {
        Optional<Stock> optionalStock = stockRepo.findById(stock.getId());
        if (optionalStock.isPresent()) {
            Stock stock1 = optionalStock.get();
            stock1.setTimestamp(stock.getTimestamp());
            stock1.setQuantity(stock.getQuantity());
            return stockRepo.save(stock1);
        }
        return null;

    }

    @Override
    public List<Stock> top3HighAvailabilityProducts(LocalDateTime start, LocalDateTime end) {
        Pageable sortedByQuantityDesc = PageRequest.of(0, 3, Sort.by("quantity").descending());
        return stockRepo.findByTimestamp(start, end, sortedByQuantityDesc).stream().collect(Collectors.toList());
    }

    @Override
    public void save(Stock stock) {
        stockRepo.save(stock);
    }
}
