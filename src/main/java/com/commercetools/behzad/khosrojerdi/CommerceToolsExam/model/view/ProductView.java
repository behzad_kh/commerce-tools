package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductView {
    private String productId;
    private LocalDateTime requestTimestamp;
    private Stock stock = new Stock();
}
