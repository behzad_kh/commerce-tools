package com.commercetools.behzad.khosrojerdi.CommerceToolsExam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommerceToolsExamReactiveMySqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommerceToolsExamReactiveMySqlApplication.class, args);
    }

}
