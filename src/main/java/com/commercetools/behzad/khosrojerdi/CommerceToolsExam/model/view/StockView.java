package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockView {

    private String id;
    private LocalDateTime timestamp;
    private String productId;
    private Integer quantity;
}
