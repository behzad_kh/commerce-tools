package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DynamicUpdate
public class Stock {
    @Id
    private String id;

    private LocalDateTime timestamp;

    private Integer quantity;

    @OneToOne(cascade = CascadeType.ALL)
    private Product product;


    @Version
    private Integer version;

    public Stock(String id, LocalDateTime timestamp, int quantity) {
        this.id = id;
        this.quantity = quantity;
        this.timestamp = timestamp;
    }
}
