package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductStatistic {
    private String productId;
    private Integer itemsSold;

}
