package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.util;


import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view.ProductStatistic;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view.ProductView;

public class ProductConverter {

    private ProductConverter() {

    }

    public static ProductView convertToProductView(Product product) {
        ProductView productView = new ProductView();
        productView.setProductId(product.getId());
        productView.setRequestTimestamp(product.getRequestTimestamp());
        if (product.getStock() != null) {
            productView.getStock().setId(product.getStock().getId());
            productView.getStock().setQuantity(product.getStock().getQuantity());
            productView.getStock().setTimestamp(product.getStock().getTimestamp());
        }
        return productView;
    }

    public static ProductStatistic convertToProductStatistic(Product product) {
        ProductStatistic productStatistic = new ProductStatistic();
        productStatistic.setProductId(product.getId());
        productStatistic.setItemsSold(product.getItemSold());
        return productStatistic;
    }
}
