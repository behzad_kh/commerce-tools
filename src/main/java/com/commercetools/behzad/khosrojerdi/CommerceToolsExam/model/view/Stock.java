package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stock {
    private String id;
    private LocalDateTime timestamp;
    private Integer quantity;

}
