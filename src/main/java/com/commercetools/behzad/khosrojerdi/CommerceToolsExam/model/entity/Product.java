package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DynamicUpdate
public class Product {

    @Id
    private String id;

    private LocalDateTime requestTimestamp;

    @OneToOne(mappedBy = "product")
    private Stock stock;

    private Integer itemSold;
}
