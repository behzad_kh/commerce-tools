package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Statistics {

    private LocalDateTime requestTimeStamp;
    private String range;
    private List<StockView> topAvailableProducts = new ArrayList<>(3);
    private List<ProductStatistic> topSellingProducts = new ArrayList<>(3);


}
