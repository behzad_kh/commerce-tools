package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.controller;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.exception.InputsNotValidException;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.exception.NotFoundException;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view.ProductView;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view.Statistics;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view.StockView;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service.ProductService;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service.StockService;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.util.ProductConverter;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.util.StockConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@Slf4j
public class StockController {

    @Autowired
    private StockService stockService;

    @Autowired
    private ProductService productService;

    @PostMapping(value = "/updateStock", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void updateStock(@RequestBody StockView stockView) {

        Stock stock = stockService.updateStock(StockConverter.convertToStock(stockView));
        if (stock == null) {
            throw new NotFoundException("StockId=" + stockView.getId() + " (Not Found) ");
        }
    }

    @GetMapping("/stock")
    public ProductView getStock(@RequestParam("productId") String productId) {
        log.info("product Id:{}", productId);
        Product product = productService.findById(productId);
        if (product != null) {
            return ProductConverter.convertToProductView(product);
        } else {
            throw new NotFoundException("ProductId=" + productId + " (Not Found) ");
        }
    }


    @GetMapping("/statistics")
    public Statistics getStatistics(@RequestParam("time") String time) {
        log.info("time:{}", time);
        Statistics statistics = new Statistics();
        statistics.setRequestTimeStamp(LocalDateTime.now());
        if (checkDateTimeInput(time.toLowerCase())) {
            statistics.setRange(time);
            LocalDateTime end = LocalDateTime.now();
            LocalDateTime start = time.equalsIgnoreCase("today") ?
                    LocalDateTime.now().minusDays(1).withHour(23).withMinute(59).withSecond(59) :
                    LocalDateTime.now().minusMonths(1).minusDays(1).withHour(23).withMinute(59).withSecond(59);//last month
            stockService.top3HighAvailabilityProducts(start, end).forEach(stock -> {
                statistics.getTopAvailableProducts().add(StockConverter.convertToStockView(stock));
            });
            productService.top3SellingProducts(start, end).forEach(product -> {
                statistics.getTopSellingProducts().add(ProductConverter.convertToProductStatistic(product));
            });
        } else {
            throw new InputsNotValidException("Time is not valid in range[today,lastMonth]");
        }
        return statistics;
    }


    private boolean checkDateTimeInput(String dateTime) {
        return dateTime.equalsIgnoreCase("today") || dateTime.equalsIgnoreCase("lastmonth");
    }


}
