package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface ProductRepository extends JpaRepository<Product, String> {

    @Query("select p from Product p inner join p.stock s where s.timestamp >= :start and s.timestamp <= :end ")
    Page<Product> findByTimestamp(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, Pageable page);

}
