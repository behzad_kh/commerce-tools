package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;

import java.time.LocalDateTime;
import java.util.List;

public interface ProductService {

    Product findById(String productId);

    List<Product> top3SellingProducts(LocalDateTime start, LocalDateTime end);

    Product save(Product product);
}
