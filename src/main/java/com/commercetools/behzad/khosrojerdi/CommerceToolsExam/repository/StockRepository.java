package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface StockRepository extends JpaRepository<Stock, String> {

    @Query("select s from Stock s where s.timestamp between :start and :end ")
    Page<Stock> findByTimestamp(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end, Pageable page);

}
