package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.util;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.view.StockView;


public class StockConverter {

    private StockConverter() {
    }

    public static StockView convertToStockView(Stock stock) {
        StockView stockView = new StockView();
        stockView.setId(stock.getId());
        Product product = stock.getProduct();
        stockView.setProductId(product != null ? product.getId() : null);
        stockView.setQuantity(stock.getQuantity());
        stockView.setTimestamp(stock.getTimestamp());
        return stockView;
    }

    public static Stock convertToStock(StockView stockView) {
        Stock stock = new Stock();
        stock.setId(stockView.getId());
        Product product = new Product();
        product.setId(stockView.getProductId());
        stock.setProduct(product);
        stock.setQuantity(stockView.getQuantity());
        stock.setTimestamp(stockView.getTimestamp());
        return stock;
    }
}
