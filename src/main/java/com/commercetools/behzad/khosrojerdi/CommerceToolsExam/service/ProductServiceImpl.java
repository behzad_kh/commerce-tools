package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Product;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository.ProductRepository;
import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepo;

    @Autowired
    private StockRepository stockRepo;

    @Override
    public Product findById(String productId) {

        Product product = null;
        Optional<Product> optionalProduct = productRepo.findById(productId);
        if (optionalProduct.isPresent()) {
            product = optionalProduct.get();
        }
        return product;
    }

    @Override
    public List<Product> top3SellingProducts(LocalDateTime start, LocalDateTime end) {
        Pageable sortedByItemSoldDesc = PageRequest.of(0, 3, Sort.by("itemSold").descending());
        return productRepo.findByTimestamp(start, end, sortedByItemSoldDesc).stream().collect(Collectors.toList());
    }

    @Override
    public Product save(Product product) {
        return productRepo.save(product);
    }
}
