package com.commercetools.behzad.khosrojerdi.CommerceToolsExam.service;

import com.commercetools.behzad.khosrojerdi.CommerceToolsExam.model.entity.Stock;

import java.time.LocalDateTime;
import java.util.List;

public interface StockService {

    Stock updateStock(Stock stock);

    List<Stock> top3HighAvailabilityProducts(LocalDateTime start, LocalDateTime end);

    void save(Stock stock);
}
