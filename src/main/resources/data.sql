
insert into product (id, item_sold, request_timestamp)
values ( 'pro001', 10, "2019-05-15 06:07:05.428000");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s001', 100, "2019-05-15 06:07:05.428000", 0, 'pro001');

insert into product (id, item_sold, request_timestamp)
values ( 'pro002', 20, "2019-05-17 06:07:05.428000");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s002', 200, "2019-05-17 06:07:05.428000", 0, 'pro002');

insert into product (id, item_sold, request_timestamp)
values ( 'pro003', 30, "2019-05-17 06:07:05.428000");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s003', 300, "2019-05-17 06:07:05.428000", 0, 'pro003');

insert into product (id, item_sold, request_timestamp)
values ( 'pro004', 40, "2019-05-17 06:07:05.428000");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s004', 400, "2019-05-17 06:07:05.428000", 0, 'pro004');

insert into product (id, item_sold, request_timestamp)
values ( 'pro005', 50, "2019-05-17 06:07:05.428000");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s005', 500, "2019-05-17 06:07:05.428000", 0, 'pro005');

insert into product (id, item_sold, request_timestamp)
values ( 'pro006', 60, "2019-05-17 06:07:05.428000");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s006', 600, "2019-05-17 06:07:05.428000", 0, 'pro006');

insert into product (id, item_sold, request_timestamp)
values ( 'pro007', 70, "2019-05-17 06:07:05.428000");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s007', 700, "2019-05-17 06:07:05.428000", 0, 'pro007');

insert into product (id, item_sold, request_timestamp)
values ( 'pro008', 5, "2019-04-16 22:54:01.754");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s008', 800, "2019-04-16 22:54:01.754", 0, 'pro008');

insert into product (id, item_sold, request_timestamp)
values ( 'pro009', 4, "2018-07-16 22:54:01.754");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s009', 900, "2018-07-16 22:54:01.754", 0, 'pro009');

insert into product (id, item_sold, request_timestamp)
values ( 'pro010', 2, "2017-07-16 22:54:01.754");
insert into stock (id,  quantity, timestamp, version, product_id)
values ('s010', 1000, "2017-07-16 22:54:01.754", 0, 'pro010');